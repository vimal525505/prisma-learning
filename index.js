const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

async function main(pageNumber, pageSize) {
  // Add one user
  // await prisma.user.create({
  //   data: {
  //     name: 'Bob',
  //     email: 'bob@prisma.io',
  //     posts: {
  //       create: { title: 'Hello World' },
  //     },
  //     profile: {
  //       create: { bio: 'I like turtles' },
  //     },
  //   },
  // })


  // Add many users
  // const newUsers = await prisma.user.createMany({data: [
  //   {name: "darshit", email: "darshit@gmail.com"},
  //   {name: "zakir", email: "zakir@gmail.com"},
  // ]});
  // console.log("newUsers :: ", newUsers);  // { count: 2 }


  // Find users
  // const allUsers = await prisma.user.findMany({
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  // })
  // console.dir(allUsers, { depth: null })


  // Pagination 
  // const offset = (pageNumber - 1) * pageSize;
  // const allUsers = await prisma.user.findMany({
  //   skip: offset,
  //   take: pageSize,
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  // })
  // console.dir(allUsers, { depth: null })

  
  // Select fields in find
  // const allUsers = await prisma.user.findMany({
  //   select: {
  //     id: 1,
  //     email: 1,
  //     name: 1, 
  //   },
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  // })
  // console.dir(allUsers, { depth: null })


  // Filtering data
  // const allUsers = await prisma.user.findMany({
  //   where: {
  //     email: {contains: "gmail.com"},
  //     name: {startsWith: "Abc"},
  //   },
  //   select: {
  //     id: 1,
  //     email: 1,
  //     name: 1, 
  //   },
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  //   orderBy: {
  //     name: 'asc',
  //   }
  // })
  // console.dir(allUsers, { depth: null })


  // Sorting data
  // const allUsers = await prisma.user.findMany({
  //   select: {
  //     id: 1,
  //     email: 1,
  //     name: 1, 
  //   },
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  //   orderBy: {
  //     name: 'asc',
  //   }
  // })
  // console.dir(allUsers, { depth: null })


  // Find Single User
  const user = await prisma.user.findUnique({ where: { id: 4 } })
  console.log("user :: ", user);


  // Update user
  // const post = await prisma.post.update({
  //   where: { id: 1 },
  //   data: { published: true },
  // })
  // console.log(post)


  // Update many users
  // const updatedUsers = await prisma.user.updateMany({
  //   where: {
  //     email: { in: ['darshit@gmail.com', 'zakir@gmail.com'] }
  //   },
  //   data: {
  //     name: "updatemany testing"
  //   }
  // });
  // console.log('updatedUsers: ', updatedUsers);  // updatedUsers: {count: 2}


  // Delete user
  // const deletedUser = await prisma.user.delete({where: { id: 5}});
  // console.log('deletedUser: ', deletedUser);  // deletedUser:  { id: 5, email: 'zakir@gmail.com', name: 'updatemany testing' }


  // Delete Many users
  // const deletedUsers = await prisma.user.deleteMany({
  //   where: {
  //     email: {in: ['test1@gmail.com', 'test2@gmail.com']}
  //   }
  // });
  // console.log("deletedUsers :: ", deletedUsers);  // { count: 2 }


}

main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })